package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.repositiry.TaskRepository;

import javax.xml.bind.ValidationException;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task persistService(String projectId, String name) throws ValidationException {
        if (projectId == null || projectId.isEmpty()) {
            throw new ValidationException("YOU ENTER AN INCORRECT VALUE");
        }
        return taskRepository.persist(projectId, name);
    }

    public void findOneService(String projectId) throws ValidationException {
        if (projectId == null || projectId.isEmpty()) {
            throw new ValidationException("YOU ENTER AN INCORRECT VALUE");
        }
        taskRepository.findOne(projectId);
    }

    public void findAllService() throws ValidationException {
        if (taskRepository.findAll().isEmpty()) {
            throw new ValidationException("LIST EMPTY");
        }
        taskRepository.findAll();
    }

    public void removeService(String taskId) throws ValidationException {
        if (taskId == null || taskId.isEmpty()) {
            throw new ValidationException("YOU ENTER AN INCORRECT VALUE");
        }
        taskRepository.remove(taskId);
    }

    public void removeTaskByIdProjectService(String projectId) {
        taskRepository.removeTaskByIdProject(projectId);
    }

    public boolean updateService(String taskId, String name) throws ValidationException {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new ValidationException("YOU ENTER AN INCORRECT VALUE");
        }
        return taskRepository.update(taskId, name);
    }

    public void mergeService(String ProjectId, String taskId, String name) throws ValidationException {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new ValidationException("YOU ENTER AN INCORRECT VALUE");
        }
        taskRepository.merge(ProjectId, taskId, name);
    }
}
