package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.repositiry.ProjectRepository;

import javax.xml.bind.ValidationException;
import java.util.List;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project persistService(String projectName) throws ValidationException {
        if (projectName == null || projectName.isEmpty()){
            throw new ValidationException("YOU ENTER AN INCORRECT VALUE");
        }
        return projectRepository.persist(projectName);
    }

    public List<Project> findAllServices() throws ValidationException {
        if (projectRepository.findAll().isEmpty()) {
            throw new ValidationException("PROJECT LIST EMPTY");
        }
        return projectRepository.findAll();
    }

    public void removeAllService() throws ValidationException {
        if (projectRepository.getProjectsList().isEmpty()) {
            throw new ValidationException("PROJECT LIST EMPTY");
        }
        projectRepository.removeAll();
    }

    public boolean removeService(String projectId) throws ValidationException {
        if (projectId == null || projectId.isEmpty()) {
            throw new ValidationException("THE PROJECT IS NOT DELETED, YOU HAVE TRANSFERRED AN INCORRECT VALUE");
        }
        return projectRepository.remove(projectId);
    }

    public void updateService(String projectId, String name) throws ValidationException {
        if (projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) {
            throw new ValidationException("THE PROJECT IS NOT EDIT, YOU HAVE TRANSFERRED AN INCORRECT VALUE");
        }
        projectRepository.update(projectId, name);
    }

    public void mergeService(String projectId, String name) throws ValidationException {
        if (projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) {
            throw new ValidationException("THE PROJECT IS NOT MERGE, YOU HAVE TRANSFERRED AN INCORRECT VALUE");
        }
        projectRepository.merge(projectId, name);
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }
}
