package ru.habibrahmanov.tm.view;

import ru.habibrahmanov.tm.repositiry.ProjectRepository;
import ru.habibrahmanov.tm.repositiry.TaskRepository;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.TaskService;

import javax.xml.bind.ValidationException;
import java.util.Scanner;

public class Menu {

    private ProjectService projectService;
    private ProjectRepository projectRepository;
    private TaskService taskService;
    private TaskRepository taskRepository;
    private Scanner scanner;
    private String projectId;
    private String taskId;
    private String name;

    public Menu(ProjectService projectService, ProjectRepository projectRepository, TaskService taskService, TaskRepository taskRepository, Scanner scanner) {
        this.projectService = projectService;
        this.projectRepository = projectRepository;
        this.taskService = taskService;
        this.taskRepository = taskRepository;
        this.scanner = scanner;
    }

    //persist (ProjectRepository)
    public void commandCreatePtoject() throws ValidationException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        name = scanner.nextLine();
        projectService.persistService(name);
        System.out.println("CREATE NEW PROJECT SUCCESSFULLY");
    }

    public void commandShowProject() throws ValidationException {
        System.out.println("[PROJECT LIST]");
        projectService.findAllServices();
        for (int i = 0; i < projectService.findAllServices().size(); i++) {
            System.out.println("ID: " + projectService.findAllServices().get(i).getId() + " / NAME: " + projectService.findAllServices().get(i).getName());
        }
    }

    public void commandRemoveProject() throws ValidationException {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER ID PROJECT:");
        projectId = scanner.nextLine();
        projectService.removeService(projectId);
        taskService.removeTaskByIdProjectService(projectId);
        System.out.println("PROJECT REMOVED SUCCESSFULLY");
    }

    public void commandEditProject() throws ValidationException {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER ID");
        projectId = scanner.nextLine();
        System.out.println("ENTER NAME TO CHANGE");
        name = scanner.nextLine();
        projectService.updateService(projectId, name);
        System.out.println("PROJECT EDIT SUCCESSFULLY");
    }

    public void commandClearProject() throws ValidationException {
        System.out.println("[PROJECT CLEAR]");
        projectService.removeAllService();
        System.out.println("DELETE ALL PROJECTS");
    }

    public void commandMergeProject() throws ValidationException {
        System.out.println("[PROJECT MERGE]");
        System.out.println("ENTER PROJECT ID");
        projectId = scanner.nextLine();
        System.out.println("ENTER PROJECT NAME");
        name = scanner.nextLine();
        projectService.mergeService(projectId, name);
        System.out.println("PROJECT MERGE SUCCESSFULLY");
    }

    public void commandCreateTask() throws ValidationException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER PROJECT ID");
        projectId = scanner.nextLine();
        System.out.println("ENTER TASK NAME");
        name = scanner.nextLine();
        taskService.persistService(projectId, name);
        System.out.println("TASK CREATE SUCCESSFULLY");
    }

    public void commandShowTask() throws ValidationException {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER PROJECT ID]");
        projectId = scanner.nextLine();
        taskService.findOneService(projectId);
        for (int i = 0; i < taskRepository.findOne(projectId).size(); i++) {
            System.out.println("ID: " + taskRepository.findOne(projectId).get(i).getId() + " / NAME: " + taskRepository.findOne(projectId).get(i).getName());
        }
    }

    public void commandShowAllTask() throws ValidationException {
        System.out.println("[SHOW ALL TASK]");
        taskService.findAllService();
        for (int i = 0; i < taskRepository.findAll().size(); i++) {
            System.out.println("TASK ID: " + taskRepository.findAll().get(i).getId() + " / NAME: " + taskRepository.findAll().get(i).getName());
        }
    }

    public void commandRemoveTask() throws ValidationException {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER ID TASK:");
        taskId = scanner.nextLine();
        taskService.removeService(taskId);
        System.out.println("TASK REMOVED SUCCESSFULLY");
    }

    public void commandEditTask() throws ValidationException {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID:");
        taskId = scanner.nextLine();
        System.out.println("ENTER TASK NAME:");
        name = scanner.nextLine();
        taskService.updateService(taskId, name);
        System.out.println("TASK UPDATED SUCCESSFULLY");
    }

    public void commandMergeTask() throws ValidationException {
        System.out.println("[TASK MERGE]");
        System.out.println("ENTER PROJECT ID:");
        projectId = scanner.nextLine();
        System.out.println("ENTER TASK ID:");
        taskId = scanner.nextLine();
        System.out.println("ENTER TASK NAME:");
        name = scanner.nextLine();
        taskService.mergeService(projectId, taskId, name);
        System.out.println("TASK MERGE SUCCESSFULLY");
    }

    public void commandHelpProject() {
        System.out.println("help: Show all commands");
        System.out.println("[pcl] project-clear: Remove all project");
        System.out.println("[pr] project-remove: Remove selected project");
        System.out.println("[pcr] project-create: Create new projects");
        System.out.println("[pl] project-list: Shaw all project");
        System.out.println("[ps] project-switch: Switch current project");
        System.out.println("[tcr] task-create: Create new task");
        System.out.println("[tl] task-list: Show all tasks");
        System.out.println("[tr] task-remove: Remove selected task");
        System.out.println("[te] task-edit: Edit selected task");
    }

}
