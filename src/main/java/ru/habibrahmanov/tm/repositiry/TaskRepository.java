package ru.habibrahmanov.tm.repositiry;

import ru.habibrahmanov.tm.entity.Task;
import java.util.*;
import java.util.stream.Collectors;

public class TaskRepository {
    private List<Task> taskList = new ArrayList<>();

    public Task persist(String projectId, String name) {
        Task task = new Task(name, UUID.randomUUID().toString(), projectId);
        taskList.add(task);
        return task;
    }

    public List<Task> findOne(String projectId) {
        return taskList
                .stream()
                .filter(e -> e.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

    public List<Task> findAll() {
        return taskList;
    }

    public void remove(String taskId) {
        for (int i = 0; i < taskList.size(); i++) {
            if (taskId.equals(taskList.get(i).getId())) {
                taskList.remove(taskList.get(i));
            }
        }
    }

    public boolean removeTaskByIdProject(String projectId) {
        Iterator<Task> taskIterator = taskList.iterator();
        while (taskIterator.hasNext()) {
            Task task = taskIterator.next();
            if (task.getProjectId().equals(projectId)) {
                taskIterator.remove();
                return true;
            }
        }
        return false;
    }

    public boolean update(String taskId, String name) {
        for (int i = 0; i < taskList.size(); i++) {
            if (taskId.equals(taskList.get(i).getId())) {
                taskList.get(i).setName(name);
                return true;
            }
        }
        return false;
    }

    public void merge(String projectId, String taskId, String name) {
        boolean flag = false;
        for (int i = 0; i < taskList.size(); i++) {
            if (taskId.equals(taskList.get(i).getId())) {
                update(taskId, name);
                flag = true;
            }
        }
        if (!flag) {
            persist(projectId, name);
        }
    }
}

