package ru.habibrahmanov.tm.repositiry;

import ru.habibrahmanov.tm.entity.Project;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class ProjectRepository {
    private List<Project> projectsList = new ArrayList<>();

    public List<Project> getProjectsList() {
        return projectsList;
    }

    public void setProjectsList(List<Project> projectsList) {
        this.projectsList = projectsList;
    }

    // createProject
    public Project persist(String name) {
        Project project = new Project(name, UUID.randomUUID().toString());
        projectsList.add(project);
        return project;
    }

    public List<Project> findAll() {
        return projectsList;
    }

    public void removeAll() {
        projectsList.clear();
    }

    public boolean remove(String projectId) {
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId.equals(projectsList.get(i).getId())) {
                projectsList.remove(projectsList.get(i));
                return true;
            }
        }
        return false;
    }

    // if there is a project, edit it, if not, create
    public void merge(String projectId, String name) {
        boolean flag = false;
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId.equals(projectsList.get(i).getId())) {
                update(projectId, name);
                flag = true;
            }
        }
        if (!flag) {
            persist(name);
        }
    }

    public boolean update(String projectId, String name) {
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId.equals(projectsList.get(i).getId())) {
                projectsList.get(i).setName(name);
                return true;
            }
        }
        return false;
    }
}

