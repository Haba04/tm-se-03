package ru.habibrahmanov.tm.entity;

import java.util.Date;

public class Task {
    private String name;
    private String description;
    private String id;
    private String projectId;
    private Date beginTaskDate;
    private Date endTaskDate;

    public Task(String name, String id, String projectId) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Date getBeginTaskDate() {
        return beginTaskDate;
    }

    public void setBeginTaskDate(Date beginTaskDate) {
        this.beginTaskDate = beginTaskDate;
    }

    public Date getEndTaskDate() {
        return endTaskDate;
    }

    public void setEndTaskDate(Date endTaskDate) {
        this.endTaskDate = endTaskDate;
    }
}
