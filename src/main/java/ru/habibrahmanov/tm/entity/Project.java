package ru.habibrahmanov.tm.entity;

import java.util.Date;

public class Project {
    private String name;
    private String description;
    private String id;
    private Date beginProjectDate;
    private Date endProjectDate;

    public Project(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginProjectDate() {
        return beginProjectDate;
    }

    public void setBeginProjectDate(Date beginProjectDate) {
        this.beginProjectDate = beginProjectDate;
    }

    public Date getEndProjectDate() {
        return endProjectDate;
    }

    public void setEndProjectDate(Date endProjectDate) {
        this.endProjectDate = endProjectDate;
    }
}
